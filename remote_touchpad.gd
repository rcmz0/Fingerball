extends Node

onready var finger_manager = get_node('/root/Root/FingerManager')

func _ready():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client('192.168.1.11', 42069)
	get_tree().network_peer = peer

remote func set_touches(touches):
	for i in touches.size():
		var touch = touches[i]
		touch.x /= 2160
		touch.y /= 1080
		touch = touch*2 - Vector2.ONE
		touches[i] = touch
	finger_manager.set_touches(touches)
