magic-tracking README
=====================

magic-tracking is a UDP/OSC server written in pure Python that broacasts low-level events from Apple's magic mouses/trackpads (for each contact: center, small/large ellipsis axes, orientation).


Server
------

The server takes the following arguments from the command line::

	% ./server.py --help
	option --help not recognized
	Usage: server.py [-lh:p:t:] [<d0>, ...]
		-l --list         print the number of trackable devices, then exit
		-h --host <host>  target host (defaults to "localhost")
		-p --port <port>  target port (defaults to 3333)
		-t --type <type>  message type ("udp"|"osc", defaults to "udp")
		<d0>, ...         devices to track (all device if none given)


The packets have the following structure:

- UDP : 1 text line per event:: 
	
	magic <device_id> <contact_id> <state> <timestamp> <x> <y> <size> <angle> <major> <minor>

- OSC : a message at /magic per event, grouped into bundles. Each message consists in::

	<device_id> <contact_id> <state> <timestamp> <x> <y> <size> <angle> <major> <minor>

The values <device_id> <contact_id> and <state> are integers; <timestamp> <x> <y> <size> <angle> <major> and <minor> are floats.


Clients
-------

An OSC and an UDP Python client are given that echo the messages to the console are given.
