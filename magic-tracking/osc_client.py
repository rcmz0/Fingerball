#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
sample osc client for magic tracker

Copyright (c) 2011, IIHM/LIG - Renaud Blanch <http://iihm.imag.fr/blanch/>
Licence: GPLv3 or higher <http://www.gnu.org/licenses/gpl.html>
"""

# imports ####################################################################

import sys
import getopt

from OSC import OSCServer


# listening ##################################################################

OSC_ADDRESS = "/magic"
def magic_handler(addr, tags, data, client_address):
	assert addr == OSC_ADDRESS
	(device_id, contact_id, state, timestamp, 
	 x, y, size, angle, major, minor) = data
	print device_id, contact_id, x, y
	
def listen(server):
	osc_server = OSCServer(server)
	osc_server.addMsgHandler(OSC_ADDRESS, magic_handler)
	
	while True:
		try:
			osc_server.handle_request()
		except KeyboardInterrupt:
			break
	

# main #######################################################################

DEFAULT_HOST = "localhost"
DEFAULT_PORT = 3333

def exit_usage(name, message=None, code=0):
	from textwrap import dedent
	usage = dedent("""\
	Usage: %s [-h:p:]
		-h --host <host>  target host (defaults to "%s")
		-p --port <port>  target port (defaults to %s)
	""")
	if message:
		sys.stderr.write("%s\n" % message)
	sys.stderr.write(usage % (name, DEFAULT_HOST, DEFAULT_PORT))
	sys.exit(code)


def main(argv=None):
	if argv is None:
		argv = sys.argv
	name, args = argv[0], argv[1:]
	
	try:
		opts, args = getopt.getopt(args, "h:p:", ["host=", "port="])
	except getopt.GetoptError, message:
		exit_usage(name, message, 1)
	
	host = DEFAULT_HOST
	port = DEFAULT_PORT
	
	for opt, value in opts:
		if opt in ["-h", "--host"]:
			host = value
		elif opt in ["-p", "--port"]:
			port = int(value)
	
	listen((host, port))
		

if __name__ == "__main__":
	sys.exit(main())
