extends Area

onready var congratulation_label:Control = get_node("CongratulationLabel")
onready var start_position = transform.origin

func _ready():
	randomize()
	randomize_position()
	connect("body_entered", self, "on_body_entered")

func on_body_entered(body:Node):
	if body.name == "Ball":
		congratulation_label.show()
		yield(get_tree().create_timer(1), "timeout")
		get_tree().reload_current_scene()

func randomize_position():
	transform.origin.x = rand_range(-start_position.x, +start_position.x)
