extends Spatial

var LERP_SPEED = 20
var LEG_LENGTH = 1.5

var FOOT_TOUCHING_Y = 0.2
var FOOT_LIFTED_Y = 0.5

onready var finger_manager = get_node('/root/Root/FingerManager')
onready var magick_tracking = get_node('/root/Root/MagickTrackingClient')
onready var left_foot = get_node("LeftFoot")
onready var right_foot = get_node("RightFoot")
onready var body = get_node("Body")

func xz_to_xyz(vector:Vector2) -> Vector3:
	return Vector3(vector.x, 0, vector.y)

func xyz_to_xz(vector:Vector3) -> Vector2:
	return Vector2(vector.x, vector.z)

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _physics_process(delta):
	# joystick camera movement
#	var camera_vector = Input.get_vector('camera_left', 'camera_right', 'camera_down', 'camera_up')
#	rotate_y(-camera_vector.x / 30.0)
#
	# joystick/wasd player movement
#	var move_vector = Input.get_vector('move_left', 'move_right', 'move_backward', 'move_forward')
#	var target_velocity = global_transform.basis * Vector3(-move_vector.x, 0, move_vector.y) * SPEED
#	velocity = velocity.linear_interpolate(target_velocity, delta * ACCELERATION)
#	move_and_slide(velocity)
	
	# compute feet height
	var left_foot_y = FOOT_TOUCHING_Y if finger_manager.left.touching else FOOT_LIFTED_Y
	var right_foot_y = FOOT_TOUCHING_Y if finger_manager.right.touching else FOOT_LIFTED_Y
	
	# move feet up/down 
	left_foot.move_and_slide(Vector3(0, left_foot_y - left_foot.transform.origin.y, 0) * LERP_SPEED)
	right_foot.move_and_slide(Vector3(0, right_foot_y - right_foot.transform.origin.y, 0) * LERP_SPEED)
	
	# turn the body
	var finger_total_delta = finger_manager.left.total_delta + finger_manager.right.total_delta
	var rotation_angle = finger_total_delta.angle_to(Vector2.DOWN)
	if abs(rotation_angle) > TAU/6: rotation_angle = 0
	body.rotate_y(rotation_angle*finger_total_delta.length()/20)
	
	# turn the body with finger orientation
#	body.rotate_y(magick_tracking.angle/10)
	
	# compute the translation
	var translate_delta = Vector2.ZERO
	if finger_manager.left.touching and not finger_manager.right.touching:
		if finger_manager.left.previous_touching:
			translate_delta = finger_manager.left.position - finger_manager.left.previous_position
	elif not finger_manager.left.touching and finger_manager.right.touching:
		if finger_manager.right.previous_touching:
			translate_delta = finger_manager.right.position - finger_manager.right.previous_position
	
	# apply the translation
	translate_delta = body.transform * xz_to_xyz(translate_delta) * LEG_LENGTH
	transform.origin += translate_delta
	left_foot.transform.origin -= translate_delta
	right_foot.transform.origin -= translate_delta
	
	# move feet on the xz plane
	left_foot.move_and_slide(xz_to_xyz(xyz_to_xz(body.transform * xz_to_xyz(-finger_manager.left.position*LEG_LENGTH) - left_foot.transform.origin)) * LERP_SPEED)
	right_foot.move_and_slide(xz_to_xyz(xyz_to_xz(body.transform * xz_to_xyz(-finger_manager.right.position*LEG_LENGTH) - right_foot.transform.origin)) * LERP_SPEED)
	
	# turn feet to face the same way as the body
	left_foot.transform.basis = body.transform.basis
	right_foot.transform.basis = body.transform.basis
	
	# update finger data
	for finger in [finger_manager.left, finger_manager.right]:
		var press = not finger.previous_touching and finger.touching
		var release = finger.previous_touching and not finger.touching
		if press or release:
			finger.total_delta = Vector2.ZERO
		else:
			finger.total_delta += finger.position - finger.previous_position
		finger.previous_position = finger.position
		finger.previous_touching = finger.touching

#func _input(event):
	# mouse camera mouvement
#	if event is InputEventMouseMotion:
#		rotate_y(-event.relative.x / 500.0)

