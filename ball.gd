extends RigidBody

var start_transform
onready var player = get_node("/root/Root/Player")

func _ready():
	start_transform = transform

func _input(event):
	if event is InputEventKey and event.scancode == KEY_SPACE and event.pressed:
		transform = player.body.global_transform * start_transform
		
