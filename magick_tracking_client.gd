extends Node2D

onready var finger_manager = get_node('/root/Root/FingerManager')
var peer = PacketPeerUDP.new()
var pid

#var size = 0
#var angle = 0

func _enter_tree():
	var file = File.new()
	file.open("res://magic-tracking/server.py", File.READ)
	pid = OS.execute(file.get_path_absolute(), [], false)
	while not peer.is_listening(): peer.listen(3333)

func _exit_tree():
	OS.kill(pid)

func _process(delta):
	while peer.get_available_packet_count() > 0:
		var touches = []
		var packet = peer.get_packet().get_string_from_ascii()
		for line in packet.split("\n"):
			if line.empty(): continue
			var info = line.split(" ")
			var position = Vector2(info[5], info[6])
			position.y = 1 - position.y
			position = position*2 - Vector2.ONE
#			var size = float(info[7])
#			self.size = size
#			var angle = float(info[8]) - TAU/4
#			self.angle = angle if size > 1 else self.angle
			touches.append(position)
		finger_manager.set_touches(touches)
	update()

#func _draw():
#	var center = get_viewport_rect().size/2
#	draw_line(center, center + Vector2.UP.rotated(-angle)*size*100, Color.purple)
