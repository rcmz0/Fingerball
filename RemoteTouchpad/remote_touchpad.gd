extends Node2D

var touches = {}

func _ready():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(42069)
	get_tree().network_peer = peer

func _input(event):
	if event is InputEventScreenTouch or event is InputEventScreenDrag:
		if event is InputEventScreenDrag or event.pressed:
			touches[event.index] = event.position
		else:
			touches.erase(event.index)
		rpc('set_touches', touches.values())
		update()

func _draw():
	draw_rect(get_viewport_rect(), Color.black)
	for index in touches.keys():
		draw_circle(touches[index], get_viewport_rect().size.y/10.0, Color.from_hsv(index/10.0, 1, 1))
