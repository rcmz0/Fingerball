extends Node

func _input(event):
	if event is InputEventKey and event.scancode == KEY_R and event.pressed:
		get_tree().reload_current_scene()
