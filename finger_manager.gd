extends Node2D

class Finger:
	var touching:bool
	var position:Vector2
	var previous_position:Vector2
	var previous_touching:bool
	var total_delta:Vector2
	
	func _init(position):
		self.touching = false
		self.position = position
		self.previous_position = self.position
		self.previous_touching = self.touching
		self.total_delta = Vector2.ZERO

var left = Finger.new(Vector2.LEFT/3)
var right = Finger.new(Vector2.RIGHT/3)

func set_touches(touches:Array):
	if touches.size() == 0:
		left.touching = false
		right.touching = false
	elif touches.size() == 1:
		if left.position.distance_to(touches[0]) < right.position.distance_to(touches[0]):
			left.position = touches[0]
			left.touching = true
			right.touching = false
		else:
			right.position = touches[0]
			right.touching = true
			left.touching = false
	elif touches.size() == 2:
		var solution0 = left.position.distance_to(touches[0]) + right.position.distance_to(touches[1])
		var solution1 = left.position.distance_to(touches[1]) + right.position.distance_to(touches[0])
		if solution0 < solution1:
			left.position = touches[0]
			right.position = touches[1]
		else:
			left.position = touches[1]
			right.position = touches[0]
		left.touching = true
		right.touching = true
#	else:
#		print("more than two touches ?")

func _process(delta):
	update()

func _draw():
	pass
	
#	draw_circle((left.position+Vector2.ONE)/2 * get_viewport_rect().size, 10 if left.touching else 5, Color.red)
#	draw_circle((right.position+Vector2.ONE)/2 * get_viewport_rect().size, 10 if right.touching else 5, Color.blue)
	
#	var center = get_viewport_rect().size/2
#	var FACTOR = 100
#	draw_line(center, center + left.total_delta*FACTOR, Color.red)
#	draw_line(center, center + right.total_delta*FACTOR, Color.blue)
#	draw_line(center, center + (left.total_delta + right.total_delta)*FACTOR, Color.green)
